//
//  ModelController.swift
//

import Foundation
import ImageSlideshow

class ModelController {
    var user = UserModel(
        username: ""
    )
    
    var userCards: [AlamofireSource] = []
    
    init() {
        for url in Constants.USER_CARD_IMAGE_LINKS {
            if let image = AlamofireSource(urlString: url) {
                userCards.append(image)
            }
        }
    }
}
