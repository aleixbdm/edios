//
//  Constants.swift
//

import Foundation

class Constants {

    /**
     * A dummy authentication store containing known user names and passwords.
     */
    static let DUMMY_CREDENTIALS: [String] = [
        "master:master", "mdpa:master",
        "master:mdpa", "mdpa:mdpa"
    ]

    static let DUMMY_USERNAMES: [String] = [
        "master", "mdpa"
    ]

    static let USERNAME_END: String = "@mail.com"
    
    static let USER_CARD_IMAGE_LINKS: [String] = [
        "https://source.unsplash.com/Xq1ntWruZQI/600x800",
        "https://source.unsplash.com/NYyCqdBOKwc/600x800",
        "https://source.unsplash.com/buF62ewDLcQ/600x800",
        "https://source.unsplash.com/THozNzxEP3g/600x800",
        "https://source.unsplash.com/USrZRcRS2Lw/600x800",
        "https://source.unsplash.com/PeFk7fzxTdk/600x800",
        "https://source.unsplash.com/LrMWHKqilUw/600x800",
        "https://source.unsplash.com/HN-5Z6AmxrM/600x800",
        "https://source.unsplash.com/CdVAUADdqEc/600x800",
        "https://source.unsplash.com/AWh9C-QjhE4/600x800"
    ]
}
