//
//  BaseViewController.swift
//

import UIKit

class BaseViewController: UIViewController {

    var modelController: ModelController!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

// MARK: Perform segues
extension BaseViewController {

    public func performLoginToMainSegue() {
        if (self as? LoginViewController) != nil {
            self.performSegue(withIdentifier: "loginToMainSegue", sender: self)
        }
    }

    public func performLoginToRegisterSegue() {
        if (self as? LoginViewController) != nil {
            self.performSegue(withIdentifier: "loginToRegisterSegue", sender: self)
        }
    }

    public func performMainToPhotosSegue() {
        if (self as? MainViewController) != nil {
            self.performSegue(withIdentifier: "mainToPhotosSegue", sender: self)
        }
    }

}

// MARK: Prepare segues
extension BaseViewController {

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? BaseViewController {
            passModelControllerTo(destination: destination)
        }

        // Login to register
        if let origin = self as? LoginViewController,
            let destination = segue.destination as? RegisterViewController,
            segue.identifier == "loginToRegisterSegue" {
            destination.loginViewController = origin
        }
    }

    private func passModelControllerTo(destination: BaseViewController) {
        destination.modelController = modelController
    }

}

// MARK: Textfield actions
extension BaseViewController {

    func isUsernameCorrect(username: String) -> Bool {
        return Constants.DUMMY_USERNAMES.contains(username)
    }

    func areCredentialsCorrect(username: String, password: String) -> Bool {
        for credential in Constants.DUMMY_CREDENTIALS {
            let pieces = credential.split(separator: ":")
            if pieces[0].elementsEqual(username) && pieces[1].elementsEqual(password) {
                // Account exists, return true if the password matches.
                return true
            }
        }
        return false
    }

    func setMessageError(isHidden: Bool, message: String, validation_label: UILabel) {
        if validation_label.isHidden != isHidden {
            // Update Password Validation Label
            validation_label.text = message

            // Show Password Validation Label
            UIView.animate(withDuration: 0.25, animations: {
                validation_label.isHidden = isHidden
            })
        }
    }

    func resetMessageError(validation_label: UILabel){
        if !validation_label.isHidden {
            setMessageError(isHidden: true, message: "", validation_label: validation_label)
        }
    }

}
