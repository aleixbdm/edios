//
//  RegisterViewController.swift
//

import UIKit

class RegisterViewController: BaseViewController {

    @IBOutlet weak var register_username: UITextField!
    @IBOutlet weak var register_password: UITextField!
    @IBOutlet weak var register_phone: UITextField!
    @IBOutlet weak var register_accept_terms: CheckBox!
    
    @IBOutlet weak var username_validation_label: UILabel!
    @IBOutlet weak var password_validation_label: UILabel!
    @IBOutlet weak var phone_validation_label: UILabel!
    @IBOutlet weak var accept_terms_validation_label: UILabel!
    
    var loginViewController: LoginViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure validation label
        username_validation_label.isHidden = true
        password_validation_label.isHidden = true
        phone_validation_label.isHidden = true
        accept_terms_validation_label.isHidden = true
        
        // Configure textfields
        register_username.delegate = self
        register_password.delegate = self
        register_phone.delegate = self
        register_username.addTarget(self, action: #selector(RegisterViewController.textFieldDidChange), for: .editingChanged)
        register_password.addTarget(self, action: #selector(RegisterViewController.textFieldDidChange), for: .editingChanged)
        register_phone.addTarget(self, action: #selector(RegisterViewController.textFieldDidChange), for: .editingChanged)
        
        // Hide keyboard when tap screen
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func cancelRegister(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func attemptRegister(_ sender: UIBarButtonItem) {
        attempRegister()
    }
    
    @IBAction func checkTerms(_ sender: Any) {
        if !register_accept_terms.isChecked {
            resetMessageError(validation_label: accept_terms_validation_label)
        }
    }
    
}

// MARK: Register
extension RegisterViewController {
    
    private func attempRegister() {
        dismissKeyboard()
        
        guard let username = register_username.text else {
            return
        }
        
        // Reset errors.
        resetMessageError(validation_label: username_validation_label)
        resetMessageError(validation_label: password_validation_label)
        resetMessageError(validation_label: phone_validation_label)
        resetMessageError(validation_label: accept_terms_validation_label)
        
        var cancel: Bool = false
        var focusLabel: UITextField?
        
        let (username_valid, username_error_message) = validate(register_username)
        let (password_valid, password_error_message) = validate(register_password)
        let (phone_valid, phone_error_message) = validate(register_phone)
        let accept_terms_valid = register_accept_terms.isChecked
        
        if let message = username_error_message, !username_valid {
            focusLabel = focusLabel ?? register_username
            setMessageError(isHidden: username_valid, message: message, validation_label: username_validation_label)
            cancel = true
        }
        
        if let message = password_error_message, !password_valid {
            focusLabel = focusLabel ?? register_password
            setMessageError(isHidden: password_valid, message: message, validation_label: password_validation_label)
            cancel = true
        }
        
        if let message = phone_error_message, !phone_valid {
            focusLabel = focusLabel ?? register_phone
            setMessageError(isHidden: phone_valid, message: message, validation_label: phone_validation_label)
            cancel = true
        }
        
        if !accept_terms_valid {
            setMessageError(isHidden: accept_terms_valid, message: "You must accept terms to register.", validation_label: accept_terms_validation_label)
            cancel = true
        }
        
        if cancel {
            focusLabel?.becomeFirstResponder()
        } else {
            loginViewController?.performLoginToMainSegue()
            dismiss(animated: true, completion: nil)
            modelController.user.username = username
        }
    }
    
}

// MARK: Textfield actions
extension RegisterViewController: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        switch textField {
        case register_username:
            resetMessageError(validation_label: username_validation_label)
            break
        case register_password:
            resetMessageError(validation_label: password_validation_label)
            break
        case register_phone:
            resetMessageError(validation_label: phone_validation_label)
            break
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case register_username:
            let (valid, message) = validateFormat(textField)
            if let message = message, !valid {
                setMessageError(isHidden: valid, message: message, validation_label: username_validation_label)
            } else {
                register_password.becomeFirstResponder()
            }
            break
        case register_password:
            let (valid, message) = validateFormat(textField)
            if let message = message, !valid {
                setMessageError(isHidden: valid, message: message, validation_label: password_validation_label)
            } else {
                register_phone.becomeFirstResponder()
            }
            break
        case register_phone:
            let (valid, message) = validateFormat(textField)
            if let message = message, !valid {
                setMessageError(isHidden: valid, message: message, validation_label: phone_validation_label)
            } else {
                attempRegister()
            }
            break
        default:
            break
        }
        
        return true
    }
    
    func validateFormat(_ textField: UITextField) -> (Bool, String?) {
        guard let text = textField.text else {
            return (false, nil)
        }
        
        if textField == register_password {
            return (text.count > 3, "Your password is too short.")
        }
        
        return (text.count != 0, "This field cannot be empty.")
    }
    
    func validate(_ textField: UITextField) -> (Bool, String?) {
        let (valid, message) = validateFormat(textField)
        
        if !valid {
            return (valid, message)
        }
        
        return (true, nil)
    }
    
}
