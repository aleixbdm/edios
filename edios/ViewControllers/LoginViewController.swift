//
//  LoginViewController.swift
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var login_username: UITextField!
    @IBOutlet weak var login_password: UITextField!

    @IBOutlet weak var username_validation_label: UILabel!
    @IBOutlet weak var password_validation_label: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Configure validation label
        username_validation_label.isHidden = true
        password_validation_label.isHidden = true

        // Configure textfields
        login_username.delegate = self
        login_password.delegate = self
        login_username.addTarget(self, action: #selector(LoginViewController.textFieldDidChange), for: .editingChanged)
        login_password.addTarget(self, action: #selector(LoginViewController.textFieldDidChange), for: .editingChanged)

        // Hide keyboard when tap screen
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }

    @objc private func dismissKeyboard() {
        self.view.endEditing(true)
    }

    @IBAction func attemptLogin(_ sender: UIButton) {
        attempLogin()
    }

    @IBAction func startRegister(_ sender: UIButton) {
        performLoginToRegisterSegue()
    }

}

// MARK: Login
extension LoginViewController {
    
    private func attempLogin() {
        dismissKeyboard()

        guard let username = login_username.text else {
            return
        }

        // Reset errors.
        resetMessageError(validation_label: username_validation_label)
        resetMessageError(validation_label: password_validation_label)

        var cancel: Bool = false
        var focusLabel: UITextField?

        let (username_valid, username_error_message) = validate(login_username)
        let (password_valid, password_error_message) = validate(login_password)

        if let message = username_error_message, !username_valid {
            focusLabel = focusLabel ?? login_username
            setMessageError(isHidden: username_valid, message: message, validation_label: username_validation_label)
            cancel = true
        }

        if let message = password_error_message, !password_valid {
            focusLabel = focusLabel ?? login_password
            setMessageError(isHidden: password_valid, message: message, validation_label: password_validation_label)
            cancel = true
        }

        if let focusLabel = focusLabel, cancel {
            focusLabel.becomeFirstResponder()
        } else {
            performLoginToMainSegue()
            modelController.user.username = username
        }
    }
    
}

// MARK: Textfield actions
extension LoginViewController: UITextFieldDelegate {

    @objc func textFieldDidChange(_ textField: UITextField) {
        switch textField {
        case login_username:
            resetMessageError(validation_label: username_validation_label)
            break
        case login_password:
            resetMessageError(validation_label: password_validation_label)
            break
        default:
            break
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case login_username:
            let (valid, message) = validateFormat(textField)
            if let message = message, !valid {
                setMessageError(isHidden: valid, message: message, validation_label: username_validation_label)
            } else {
                login_password.becomeFirstResponder()
            }
            break
        case login_password:
            let (valid, message) = validateFormat(textField)
            if let message = message, !valid {
                setMessageError(isHidden: valid, message: message, validation_label: password_validation_label)
            } else {
                attempLogin()
            }
            break
        default:
            break
        }

        return true
    }

    func validateFormat(_ textField: UITextField) -> (Bool, String?) {
        guard let text = textField.text else {
            return (false, nil)
        }

        if textField == login_password {
            return (text.count > 3, "Your password is too short.")
        }

        return (text.count != 0, "This field cannot be empty.")
    }

    func validate(_ textField: UITextField) -> (Bool, String?) {
        guard let text = textField.text else {
            return (false, nil)
        }

        let (valid, message) = validateFormat(textField)

        if !valid {
            return (valid, message)
        }

        if textField == login_username {
            return (isUsernameCorrect(username: text), "This username is incorrect.")
        }

        if textField == login_password {
            if let username = login_username.text {
                return (areCredentialsCorrect(username: username, password: text), "This password is incorrect.")
            }
        }

        return (true, nil)
    }

}
