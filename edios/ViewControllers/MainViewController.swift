//
//  MainViewController.swift
//

import UIKit

class MainViewController: BaseViewController {
    
    @IBOutlet weak var mainUsername: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainUsername.text = modelController.user.username + Constants.USERNAME_END
    }

    @IBAction func startPhotos(_ sender: UIButton) {
        performMainToPhotosSegue()
    }
    
}
