//
//  PhotosViewController.swift
//

import UIKit
import ImageSlideshow

class PhotosViewController: BaseViewController {
    
    @IBOutlet weak var imageSlider: ImageSlideshow!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageSlider.setImageInputs(modelController.userCards)
        imageSlider.activityIndicator = DefaultActivityIndicator()
    }
    
    @IBAction func movePrevious(_ sender: UIButton) {
        imageSlider.setCurrentPage(imageSlider.currentPage - 1, animated: true)
    }
    
    @IBAction func moveNext(_ sender: UIButton) {
        imageSlider.setCurrentPage(imageSlider.currentPage + 1, animated: true)
    }
    
}
