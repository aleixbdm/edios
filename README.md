# Entrega Entorno de desarrollo iOS

Aplicación desarrollada en Swift 4 para 11.1. Testeada con un iphone SE y 8 plus. Las pantallas han sido diseñadas para adaptarse en orientación vertical y horizontal. Traducida en Inglés. Se requiere conexión a internet.

Login:
Al iniciar la aplicación aparece un formulario para ingresar en nombre de usuario y password. Al escribir en cada campo se ha customizado el botón nativo del teclado para saltar al siguiente campo o intentar loguearse. Si ha habido algun problema al ingresar los datos aparecen los errores correspondientes. Debajo hay los botones para loguearse y registrarse. Al pulsar el Login se dirigirá a la pantalla Main si no ha habido ningún problema, y al pulsar Register se dirigirá a la pantalla Register.

Register:
Aparece un formulario para ingresar en nombre de usuario, password y número de teléfono. También aparece un checkbox para aceptar los términos. Al escribir en cada campo se ha customizado el botón nativo del teclado para saltar al siguiente campo o intentar registrarse. Si ha habido algun problema al ingresar los datos aparecen los errores correspondientes. Debajo hay el botón para registrarse. Al pulsar el Register se dirigirá a la pantalla Main si no ha habido ningún problema.

Main:
Aparece un texto con el nombre de usuario ingresado en el Login o Register con @mail.com añadido al final. Debajo hay el botón para dirigirse a la pantalla de Photos.

Photos:
Aparece un slider con 10 fotos. Las fotos se descargan de internet al momento. Se ha utilizado el pod Alamofire para descargar las fotos con una url. Se pueden deslizar las fotos mediante swipe o pulsando los botones Left & Right de debajo las fotos gracias al pod ImageSlideShow. Se pueden deslizar las fotos infinitamente.
